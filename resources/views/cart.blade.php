<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
     body{
        height: 100%;
        margin: 0;
        overflow:hidden;
    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 32%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position:relative;
        width: 98.5%;
        height:85vh;
        max-height: auto;
        background-image: url("{{asset('img/nav/topup.gif')}}");
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 40%;
        height: 18.5%;
        max-height: 20%;
        display: inline-block;
        margin-left:30%;
        margin-bottom: 1%;
        background-color:#102236;
        color: white;
        padding: 3px;
        opacity: 95%;
    }
    .fg form {
        display:inline;
    }
    .fgfoto{
        background-color:white;
        width: 200px;
        height: 100px;
        float:left;
        margin-right: 5px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    .have{
        background-color:#4CAF50;
        color: white;
        width: auto;
        height: auto;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top:0;
        position: sticky;
    }
    .have form{
      float:right;
      padding-left: 1px;
    }
    .have input{
        text-decoration: none;
        border: none;
        bottom: 1px;
        margin-right: 4px;
        border-radius: 10px;
        background-color:#346beb;
        color: white;
    }
    #footer{
        color: #888e94;
        font-family: calibri;
        background-color: #071426;
        overflow: hidden;
        padding-left: 45%;
        padding-bottom:2%;
    }
    #cart{
        position:relative;
        background-image:url("{{asset('img/cart/cart.png')}}");
        width: 50; height:50;
    }
    .rem{
        border: none;
        color: white;
        float: right;
        position: relative;
        height:25px;
        color:transparent;
        width: 25px;
        background-color:transparent;
        margin-right: 1%;
        border-radius: 7px;
        display:inline;
        background-image: url("{{asset('img/cart/del.png')}}");
        background-repeat:no-repeat;
        background-size:100%;
        transition: width 1s;
    }
    .rem:hover{
        background-color:#e04e2d;
        color: white;
        background-image:none;
        width: 75px;
    }
    #total{
        width: 40%;
        height: auto;
        position: relative;
        margin-left:30%;
        background-color:#102236;
        color: white;
        padding: 3px;
        opacity: 95%;
        border-radius: 5px;
        display:inline-block;
    }
    #total form{
        display:inline;
    }
    #checkout{
        background-color:#4CAF50;
        color:white;
        border-radius: 5px;
        border:none;
        position:relative;
        height: 30px;
        font-size: 15px;
        float:right;
    }
    </style>
</head>
<body>
<ul>
    <form action="{{url('store/processHeader')}}" method="post" >
        @csrf
        <li><a href="{{url('store/home')}}"><img src="{{asset('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></a></li>
        <li><input type="submit" name="home" value="Store"></li>
        <li><input type="submit" name="lib" value="Library"></li>
        <li><input type="submit" name="prof" value="Profile"></li>
        <li class="active"><a style="position:absolute; float: left;" href="{{url('store/cart')}}"><img src="{{asset('img/cart/cart.png')}}" alt="" width="50" height="50"></a>
        <?php $cart=Session::get("cart")?>
            @if(Session::has("cart"))
            {{count($cart)}}
            @else
            0
            @endif
        </li>
    </form>
</ul>

@if($errors->any())
    <?php $pesan = ""; ?>
    @foreach ($errors->all() as $err)
        <?php $pesan.=$err.'\n'; ?>
    @endforeach
    <?= "<script>alert('$pesan');</script>"?>
@endif

<div id="isi">
    @if (Session::has('user'))
    <?php $user= Session::get('user')?>
    <h3 style="display:inline; position: absolute">{{$user->username}}'s Cart</h3>
    <div class="have">
        <div class="have">
            <a href="{{url('store/profile')}}"><img src="{{asset('img/profile/'.$user->foto)}}" alt="" width="55px" height="55px"></a>
            <form  action="{{url('store/processHeader')}}" method="post">
                @csrf
                Hello, <?= $user->username ?> <br> ETcoin: <?= $user->balance ?> <br>
                <input type="submit" name="out" value="Logout">
            </form>
        </div>
    </div>
    <br><br><hr>
    @if (Session::has("cart"))
    <?php $ctr=0;?>
        @foreach (Session::get("cart") as $item)
            <div class="fg">
                <div class="fgfoto">
                    <img src="{{asset('img/foto_game/'.$item->foto)}}" alt="" width="200px" height="100px">
                </div>
                <h3 style="font-family: courier">{{ $item->nama }}</h3><hr>
                Price: {{ $item->harga }}
                <form action="{{url('store/processCart')}}" method="post">
                    @csrf
                    <input type="hidden" name="game_name" value="{{ $item->nama }}">
                    <input type="hidden" name="index_game" value="{{ $ctr }}">
                    <input class="rem" type="submit" name="remove" value="Remove">
                </form>
                <?php $ctr++; ?>
            </div>
        @endforeach
        <div id="total">
        Total: {{$total}}
        <form action="{{url('store/processCart')}}" method="post">
            @csrf
            <input id="checkout" type="submit" name="confirm" value="Checkout Now">
        </form>
        </div>
    @endif
    @else
    <h3 style="display:inline; position: absolute">Nothing to show. Please login to add items here.</h3>
            <div class="have">
                Have an account?<br>
                <form action="{{url('store/processHeader')}}" method="post">@csrf
                <input type="submit" name="login" value="Login">
                <input type="submit" name="register" value="Sign Up">
                </form>
            </div>
            <br><br><hr>
    @endif
</div>
<div id="footer">
Copyright FAIPROJECT 2019.
</div>
</body>
</html>
