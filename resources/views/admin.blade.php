<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Page</title>
    <style>
    body{
        height: 100%;
        margin: 0;
        overflow:hidden;
    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 40%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position:relative;
        width: 98.5%;
        height:100vh;
        max-height: auto;
        background-image: url("{{asset('img/admin/adminback.png')}}");
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 35%;
        height: auto;
        opacity: 95%;
        display: inline-block;
        margin-left:10%;
        margin-top: 1%;
        margin-bottom: 1%;
        background-color:#102236;
        color: white;
        padding: 5px;
        float: left;
    }
    .fg form{
        position: inherit;
        float: left;
        width: 100%;
    }
    .fg h1{
        float:left;
        position: relative;
        left: 36%;
    }
    .input {
        border: none;
        background-color: transparent;
        border-bottom: solid white 1px;
        color:  #00fcbd;
        width: 100%;
    }
    .input:focus{
        border-bottom: solid green 1px;
    }
    .submit{
        border: none;
        position: relative;
        width: 100px;
        color:white;
        background-color:#4CAF50;
        border-radius: 20px
    }
    .pay{
        float:left;
        position:relative;
        border:solid #00fcbd 2px;
        background:#253d4f;
        width: 150px;
        height: 70px;
        border-radius: 10px;
        margin-left: 3.5%;
    }
    .fgfoto{
        background-color:white;
        width: 325px;
        height: 225px;
        float:left;
        margin-right: 5px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    .have{
        background-color:#4CAF50;
        color: white;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top: 0;
        position: sticky;
        text-align: center;
    }
    .have input{
        text-decoration: none;
        border: none;
        background-color:transparent;
        color: #001d6b;
    }
    #footer{
        color: #888e94;
        font-family: calibri;
        background-color: #071426;
        overflow: hidden;
        padding-left: 45%;
        padding-bottom:2%;
    }
    #ph{
        position: relative;
        float:left;
        margin-top: 5px;
        width: 100%;
        color: white;
    }
    #ph table{
        border-collapse: collapse;
        color: white;
        table-layout:fixed;
        width: 100%;
        margin-bottom: 10px;
    }
    #ph table th {
        color: black;
        background-color: #00fcbd;
        width: 100%;
    }
    #ph table td {
        text-align: center;
        border-bottom: 1px solid #346beb;
    }
    #err{
        position: absolute;
        color:#e04e2d;
        bottom: 84%;
        left: 20%;
    }
    </style>
</head>
<body>
    <ul>
    <form action="{{url('store/admin')}}" method="get" >
        <li><img src="{{asset('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></li>
        <li><input class="active" type="submit" name="admin" value="Admin"></li>
        <li><input type="submit" name="out" value="Logout"></li>
    </form>
    </ul>
<div id="isi">
    <img src="{{asset('img/admin/admin_profile.png')}}" alt="" width="50" height="50"><h1 style="display:inline;"> Admin</h1><hr>

    <div class="fg">
        <form action="{{url('store/processAdmin')}}" method="post" enctype="multipart/form-data">
        <div class="addgame">
            @csrf
            <center><h2>Add Game</h2></center>
            Name <input class="input" type="text" name="nama" id="nama"><br>
            Price  <input class="input" type="number" name="harga" id="harga"><br>
            Description  <input class="input" type="text" name="desc" id="desc"><br>
            Game Photo  <input class="input" type="file" name="foto" id="foto"><br>
            <center><input class="submit" type="submit" value="Add Game" name="addGame" id="submitGame"></center>
        </div>
        </form>
    <div id="ph">
        <table>
            <thead>
            <tr>
                <th>Game Preview</th>
                <th>Title</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $ctr =0 ?>
            @foreach($listGame as $row)
                <form action="{{url('store/processAdmin')}}" method="post">
                @csrf
                <tr>
                    <td><img src="{{asset('img/foto_game/'.$row->foto)}}" style="width:100%;height:100%"></td>
                    <td>{{$row->nama}}</td>
                    <td>{{$row->harga}}</td>
                    <td>
                    <input class="submit" type="submit" value="Delete" name="delete" style="background-color:#e04e2d">
                    <input type="submit" class="submit"  onclick="edit(this)" name="editGame" id="btnEdit" value="Edit">
                    <input type="hidden" name="nama" value="{{$row->nama}}">
                    </td>
                </tr>
                </form>
            <?php $ctr++ ?>
            @endforeach
            </tbody>
        </table>
    </div>
    <br>
    </div>

    <div class="fg">
    <center><h2>Add Banner</h2></center>
    <form action="{{url('store/processAdmin')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="addBanner">
            Caption  <input class="input" type="text" name="capBanner" id=""><br>
            Banner  <input class="input" type="file" name="fotoBanner" id=""><br>
            <center><input class="submit" type="submit" value="Add Banner" name="addBanner"><br></center>
        </div>
    </form>
    <div id="ph">
    <table>
        <tr>
            <th>Caption</th>
            <th>Banner</th>
            <th>Action</th>
        </tr>
        @foreach ($listBanner as $item)
            <form action="{{url('store/processAdmin')}}" method="post">
                @csrf
                <tr>
                    <td>{{$item->nama}}</td>
                    <td><img src="{{asset('img/gameheader/'.$item->foto)}}" width="100%" height="auto"></td>
                    <td>
                        <input class="submit" type="submit" value="Delete" name="deleteBanner" style="background-color:#e04e2d">
                        <input class="submit" type="submit" value="Edit" name="editBanner">
                        <input type="hidden" name="idBanner" value="{{$item->id}}">
                    </td>
                </tr>
            </form>

        @endforeach
    </table>
    </div>
    </div>

    <div class="fg">
    <center><h2>Add Voucher</h2></center>
    <form action="{{url('store/processAdmin')}}" method="post">
    @csrf
        Voucher Code <br>
        <input class="input" type="text" name="kode"><br>
        Ammount <br>
        <input class="input" type="text" name="jml"><br>
        <center><input class="submit" type="submit" name="addVoucher" value="Add Voucher"></center>
    </form>
    <div id="ph">
    <table>
        <tr>
            <th>Code</th>
            <th>Ammount</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        @foreach ($listV as $item)
            <form action="{{url('store/processAdmin')}}" method="post">
                @csrf
                <tr>
                    <td>{{$item->kode}}</td>
                    <td>{{$item->jumlah}}</td>
                    <td>{{$item->stat}}</td>
                    @if ($item->stat=="active")
                    <td>
                        <input class="submit" type="submit" value="Deactivate" name="deleteV" style="background-color:#e04e2d">
                        <input type="hidden" name="idV" value="{{$item->id}}">
                    </td>
                    @else
                    <td>
                        <input class="submit" type="submit" value="Activate" name="actV">
                        <input type="hidden" name="idV" value="{{$item->id}}">
                    </td>
                    @endif
                </tr>
            </form>

        @endforeach
    </table>
    </div>
    </div>
</div>
</body>
<script>
</script>
</html>
