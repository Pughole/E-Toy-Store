<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - #123</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
            background-color: #253d4f;
            color: white;
        }
        .text{
            margin-left: 15px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
        }
        tfoot tr td {
            font-weight: bold;

        }
        .invoice table {
            margin: 15px;
            border: 1;

        }
        .invoice tr td{

        }
        h3{
            margin-left: 15px;
        }
        h1{
            margin-left: 15px;
        }
        .invoice h3 h1 {

        }
        .information {
            background-color: #071426;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="invoice">
    <h1>Order Summary</h1>
    <h3>{{ date("l, j F Y h:i:s A") }}</h3>
    <div class="text">
        <b>Thank you for your purchase on E-Toy Store.</b><br>
        Your purchase has been added to your library. <br>
    </div>

    <table width="100%">
        <thead>
        <tr>
            <th>Image</th>
            <th>Description</th>=
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        <?php $ctr = 0; $total_harga=0;?>
        @foreach ($cart as $item)
            <?php $ctr++; ?>
                <tr>
                    <td><img src="{{asset('img/foto_game')."/".$item->foto}}" style="width:150px;height:100px"></td>
                    <td>{{$item->nama}}</td>
                    <td align="left">{{$item->harga}}</td>
                </tr>

        @endforeach

        </tbody>

        <tfoot>
        <tr>
            <td colspan="1"></td>
            <td align="left">Total</td>
            @foreach ($cart as $item)
                <?php $total_harga+=$item->harga ?>

            @endforeach
            <td align="left" class="gray">{{$total_harga}},-</td>
        </tr>
        </tfoot>
    </table>
</div>

<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} www.etoystore.com - All rights reserved.
            </td>
            <td align="right" style="width: 50%;">
                Proyek FAI
            </td>
        </tr>

    </table>
</div>
</body>
</html>
