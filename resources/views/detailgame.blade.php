</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    body{
        height: 100%;
        margin: 0;
        overflow:hidden;
    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 32%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position:relative;
        width: 98.5%;
        height:85vh;
        max-height: auto;
        background-image: url("{{asset('img/nav/profile.gif')}}");
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 60%;
        height: auto;
        display: inline-block;
        margin-left:20%;
        margin-top: 1%;
        margin-bottom: 1%;
        background-color:#102236;
        color: white;
        opacity: 95%;
        padding: 3px;
    }
    .fgfoto{
        background-color:white;
        position: relative;
        width: 300px;
        height: 300px;
        float:left;
        margin-right: 10px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    .have{
        background-color:#4CAF50;
        color: white;
        width: auto;
        height: auto;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top:0;
        position: sticky;
    }
    .have form{
        float:right;
        padding-left: 1px;
        margin-bottom:0;
    }
    .have input{
        text-decoration: none;
        border: none;
        bottom: 1px;
        margin-right: 4px;
        border-radius: 10px;
        background-color:#346beb;
        color: white;
    }
    #desc{
        width: inherit;
        height: 300px;
        float:left;
        position: relative;
    }
    #desc form {
        display: inline;
    }
    #desc input{
        border: none;
        margin-left: 3px;
        color:white;
        background-color:#4CAF50;
        border-radius: 20px;
        width: 20px;
        transition: width 1s;
    }
    #desc input:hover{
        width:185px;
    }
    #footer{
        color: #888e94;
        font-family: calibri;
        background-color: #071426;
        overflow: hidden;
        padding-left: 45%;
        padding-bottom:2%;
    }
    </style>
</head>
<body>
    <ul>
    <form action="{{url('store/processHeader')}}" method="post" >
        @csrf
        <li><a href="{{url('store/home')}}"><img src="{{asset('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></a></li>
        <li><input class="active" type="submit" name="home" value="Store"></li>
        <li><input type="submit" name="lib" value="Library"></li>
        <li><input type="submit" name="prof" value="Profile"></li>
        <li><a style="position:absolute; float: left;" href="{{url('store/cart')}}"><img src="{{asset('img/cart/cart.png')}}" alt="" width="50" height="50"></a>
        <?php $cart=Session::get("cart")?>
            @if(Session::has("cart"))
            {{count($cart)}}
            @else
            0
            @endif
        </li>
    </form>
    </ul>
    <div id="isi">
        @if(Session::has("user"))
        <?php $user=Session::get("user");?>
            <div class="have">
            <a href="{{url('store/profile')}}"><img src="{{asset('img/profile/'.$user->foto)}}" alt="" width="55px" height="55px"></a>
            <form  action="{{url('store/processHeader')}}" method="post">
                @csrf
                Hello, <?= $user->username ?> <br> ETcoin: <?= $user->balance ?> <br>
                <input type="submit" name="out" value="Logout">
            </form>
            </div>
        @else
            <div class="have">
            Have an account?<br>
            <form action="{{url('store/processHeader')}}" method="post">
                @csrf
                <input type="submit" name="login" value="Login">
                <input type="submit" name="register" value="Sign Up">
            </form>
            </div>
        @endif
        <br><br><hr>
        @if (isset($game))
        <div class="fg">
            <div class="fgfoto">
                <img src="{{asset('img/foto_game/'.$game->foto)}}" style="height:100%;width:100%;"><br>
            </div>
            <div id="desc">
                <h1 style="display:inline; font-family: impact;">{{$game->nama}}</h1><br>
                Price : {{$game->harga}}
                <form action="{{url('store/processDetailGame')}}" method="post">
                    @csrf
                    <input type="hidden" name="nama_game" value="{{$game->nama}}">
                    @if ($cek == 1)
                        <input type="submit" value="-   You have owned this game " name="buy" disabled>
                    @elseif ($cek == 2)
                        <input type="submit" value="-   This game is in your cart " name="buy" disabled>
                    @else
                        <input type="submit" value="+  Add this game to your cart" name="buy">
                    @endif
                </form>
                <hr>
                Description : {{$game->deskripsi}}<br>
            </div>
        </div>
        @endif
    </div>
    <div id="footer">
    Copyright FAIPROJECT 2019.
    </div>
</body>
</html>
