<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Banner</title>
    <style>
    body{
        height: 100%;
        margin: 0;
        overflow:hidden;
    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 40%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position:relative;
        width: 98.5%;
        height:100vh;
        max-height: auto;
        background-image: url("{{url('img/admin/adminback.png')}}");
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 40%;
        height: auto;
        opacity: 95%;
        display: inline-block;
        margin-left:30%;
        margin-top: 1%;
        margin-bottom: 1%;
        background-color:#102236;
        color: white;
        padding: 5px;
        float: left;
    }
    .fg form{
        position: inherit;
        float: left;
        width: 100%;
    }
    .fg h1{
        float:left;
        position: relative;
        left: 36%;
    }
    .input {
        border: none;
        background-color: transparent;
        border-bottom: solid white 1px;
        color:  #00fcbd;
        width: 100%;
    }
    .input:focus{
        border-bottom: solid green 1px;
    }
    .submit{
        border: none;
        position: relative;
        width: 100px;
        color:white;
        background-color:#4CAF50;
        border-radius: 20px
    }
    .pay{
        float:left;
        position:relative;
        border:solid #00fcbd 2px;
        background:#253d4f;
        width: 150px;
        height: 70px;
        border-radius: 10px;
        margin-left: 3.5%;
    }
    .fgfoto{
        background-color:white;
        width: 100%;
        height: 225px;
        float:left;
        margin-right: 5px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    </style>
</head>
<body>
    <ul>
    <form action="{{url('store/admin')}}" method="get" >
        <li><img src="{{url('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></li>
        <li><input type="submit" name="admin" value="Admin"></li>
        <li><input type="submit" name="out" value="Logout"></li>
    </form>
    </ul>
<div id="isi">
<img src="{{url('img/admin/admin_profile.png')}}" alt="" width="50" height="50"><h1 style="display:inline;"> Admin</h1><hr>
<div class="fg">
    <center>
    <h2>{{$banner->nama}}</h2>  
    <img src="{{asset('img/gameheader/'.$banner->foto)}}" alt=""> <br>    
    </center>    
    <form action="{{url("store/processEdBanner")}}" method="POST" enctype="multipart/form-data">
        @csrf
        Name <br> <input class="input" type="text" name="namaBanner" id="" value="{{$banner->nama}}"> <br>
        Banner Photo <br>  <input class="input" type="file" name="fotoBanner" id=""><br>
        <center>
        <input type="submit" value="Cancel" name="cancel" class="submit" style="background-color:#e04e2d">
        <input type="submit" value="Update" name="updateBanner" class="submit">
        <input type="hidden" name="idBanner" value="{{$banner->id}}">
        </center>        
    </form>
</div>
</div>    
</body>
</html>
