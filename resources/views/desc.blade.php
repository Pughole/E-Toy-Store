<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    body{
        height: 100%;
        margin: 0;
        overflow: hidden;
    }
    ul {        
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;    
        color: white;
        font-family: calibri;
        background-color: #071426;                         
    }   
    li {
        float: left;
        position: relative;
        left: 40%;
    }
    li a {
        display: block;
        color: #888e94;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }
    li a:hover:not(.active) {       
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{
        
    }
    #isi{ 
        overflow: auto;       
        margin: 0px;
        position:relative;
        width: 98.5%;
        height: 558px;
        background-color:#242322;
        font-family: calibri;        
        padding:5px 10px 5px 10px;
        color: white;     
        float: left;                         
    }
    .fg{
        border-radius: 10px;        
        position: relative;        
        width: 80%;       
        height: auto;                     
        display: inline-block;
        margin-left:10%;
        margin-top: 3%;
        margin-bottom: 1%;        
        background-color:#253d4f;
        color: white;      
        padding: 3px;
        opacity: 100%;
    }    
    .fgfoto{
        background-color:white;
        position: relative;
        width: 500px;
        height: 400px;
        float:left;
        margin-right: 10px;        
        border-top-left-radius: 10px;   
        border-bottom-left-radius: 10px;   
    }
    .have{
        background-color:#4CAF50;
        color: white;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top:0;
        position: sticky;  
    }    
    .have a{
        text-decoration: none;
        padding-right: 1px;
        display: inline-block;
    }
    #desc{        
        width: 540px;
        max-width: auto;
        height: auto;        
        float:left;        
        position: relative;
        display:inline-block;
    }   
    #footer{
        color: #888e94;
        font-family: calibri;
        background-color: #071426;                   
        overflow: hidden;                      
        padding-left: 45%;
        padding-bottom:2%;                                          
    }    
    </style>
</head>
<body>
 <ul>
    <li><img src="" alt=""></li> 
    <li><a href="home.blade.php">Store</a></li>
    <li><a href="library.blade.php">Library</a></li>
    <li><a class="active" href="profile.blade.php">Profile</a></li>
    <li><a href="">About</a></li>    
 </ul>
 <div id="isi">
    <h3 style="display:inline; position: absolute">Game Name</h3>      
    <div class="have">
    Have an account? <br>    
    <a href="">Login</a> |
    <a href="">Sign Up</a>
    </div>        
    <br><br><hr>        
    <div class="fg">
    <div class="fgfoto">    
    </div>    
    <div id="desc">
    <h1 style="display:inline; font-family: impact;">Game Title</h1> 
    <button style="border: none; margin-left: 3px; color:white; background-color:#346beb; border-radius: 20px" >+ Add this game to your cart</button>   
    <hr>    
    Game Desc
    </div>        
    </div>    
 </div> 
 <div id="footer">
   Copyright FAIPROJECT 2019.
 </div>  
</body>
</html>