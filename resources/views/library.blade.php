<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    body{
        height: 100%;
        margin: 0;
        overflow:hidden;
    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 32%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position:relative;
        width: 98.5%;
        height:85vh;
        max-height: auto;
        background-image: url("{{asset('img/nav/library.gif')}}");
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 60%;
        height: 40.5%;
        opacity: 95%;
        display: inline-block;
        margin-left:20%;
        margin-top: 3%;
        margin-bottom: 1%;
        background-color:#102236;
        color: white;
        padding: 3px;
    }
    .fgfoto{
        background-color:white;
        width: 325px;
        height: 225px;
        float:left;
        margin-right: 5px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    .have{
        background-color:#4CAF50;
        color: white;
        width: auto;
        height: auto;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top:0;
        position: sticky;
    }
    .have form{
      float:right;
      padding-left: 1px;
    }
    .have input{
        text-decoration: none;
        border: none;
        bottom: 1px;
        margin-right: 4px;
        border-radius: 10px;
        background-color:#346beb;
        color: white;
    }
    #footer{
        color: #888e94;
        font-family: calibri;
        background-color: #071426;
        overflow: hidden;
        padding-left: 45%;
        padding-bottom:2%;
    }
    #cart{
        position:relative;
        background-image:url("{{asset('img/cart/cart.png')}}");
        width: 50; height:50;
    }
    </style>
</head>
<body>
    <ul>
        <form action="{{url('store/processHeader')}}" method="post" >
            @csrf
            <li><a href="{{url('store/home')}}"><img src="{{asset('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></a></li>
            <li><input type="submit" name="home" value="Store"></li>
            <li><input class="active" type="submit" name="lib" value="Library"></li>
            <li><input type="submit" name="prof" value="Profile"></li>
            <li><a style="position:absolute; float: left;" href="{{url('store/cart')}}"><img src="{{asset('img/cart/cart.png')}}" alt="" width="50" height="50"></a>
            <?php $cart=Session::get("cart")?>
                @if(Session::has("cart"))
                {{count($cart)}}
                @else
                0
                @endif
            </li>
        </form>
    </ul>
    <div id="isi">
        @if (Session::has('user'))
        <?php $user= Session::get('user')?>
        <h3 style="display:inline; position: absolute"><?= $user->username."'s"?> Library</h3>
            <div class="have">
                <a href="{{url('store/profile')}}"><img src="{{asset('img/profile/'.$user->foto)}}" alt="" width="55px" height="55px"></a>
                <form  action="{{url('store/processHeader')}}" method="post">
                    @csrf
                    Hello, <?= $user->username ?> <br> ETcoin: <?= $user->balance ?> <br>
                    <input type="submit" name="out" value="Logout">
                </form>
            </div>
                <br><br><hr>
            @foreach ($library as $lib)
                @foreach ($game as $gm)
                    @if ($gm->nama == $lib->nama_game)
                        <div class="fg">
                            <div class="fgfoto">
                                <img src="{{asset('img/foto_game/'.$gm->foto)}}" alt="" width="325px" height="225px">
                            </div>
                        <h3 style="font-family: courier;">{{ $gm->nama }}</h3>
                        <hr>
                            Description: {{ $gm->deskripsi }} <br>
                        </div>
                    @endif
                @endforeach
            @endforeach
        @else
            <h3 style="display:inline; position: absolute">Nothing to show. Please login to see your library.</h3>
            <div class="have">
                Have an account?<br>
                <form action="{{url('store/processHeader')}}" method="post">@csrf
                <input type="submit" name="login" value="Login">
                <input type="submit" name="register" value="Sign Up">
                </form>
            </div>
            <br><br><hr>
        @endif
    </div>
    <div id="footer">
        Copyright FAIPROJECT 2019.
    </div>
</body>
</html>
