<?php

namespace App\Http\Controllers;

use App\Mail\Email;
use App\Model\UserModel;
use App\Model\GameModel;
use App\Model\BannerModel;
use App\Model\LibraryModel;
use App\Model\VoucherModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;

class MainController extends Controller
{
    public function processLogin(Request $request)
    {
        if ($request->input("loginButton")) {
            $this->validate($request, [
                "username" => "required",
                "pass" => "required"
            ]);

            if ($request->input("username") == "admin" && $request->input("pass") == "admin") {
                return redirect("store/admin");
            } else {
                //$user = DB::table('user')->where(["username"=>$request->input("username")])->first();
                $user = UserModel::where(["username" => $request->input("username")])->first();

                if ($user == null) {
                    return view("login")->withErrors(["Username salah"]);
                } else {
                    if (Hash::check($request->pass, $user->password)) {
                        if($user->verification==0){
                            Session::put("user", $user);
                            return redirect("store/home");
                        }else{
                            Session::put("userTemp", $user);
                            return redirect("store/verification");
                        }

                    } else {
                        return view("login")->withErrors(["Password salah"]);
                    }
                }
            }
        } else if ($request->input("registerButton")) {
            return redirect('store/register');
        } else {
            return redirect('store/login');
        }
    }

    public function login(Request $request)
    {
        if ($request->input('home')) {
            return redirect('store/home');
        } else if ($request->input('lib')) {
            return redirect('store/library');
        } else if ($request->input('prof')) {
            return redirect('store/profile');
        } else {
            $games = GameModel::all();
            $data["listGame"] = $games;
            return view("login", $data);
        }
    }

    public function processRegister(Request $request)
    {
        if ($request->input("btnRegister")) {
            $this->validate($request, [
                'fieldUsername' => "required|unique:user,username",
                "fieldPassword" => "required",
                "fieldConfirm" => "required|same:fieldPassword",
                "fieldEmail" => "required|email|unique:user,email"
            ], [
                "fieldEmail.email" => "Please enter a valid email!",
                "fieldConfirm.same" => "Please match your password!"
            ]);
            $user = new UserModel();
            $user->username = $request->input("fieldUsername");
            $user->password = Hash::make($request->input("fieldPassword"));
            $user->email = $request->input("fieldEmail");
            $user->foto = "default.png";
            $user->about = "Hello!";
            $user->balance = 0;
            $user->lib = "";
            $user->verification=1;
            $user->save();
            Session::put("userTemp", $user);
            return redirect("store/verification");
        } else if ($request->input("btnLogin")) {
            return redirect('store/login');
        } else {
            return redirect('store/register');
        }
    }

    public function register(Request $request)
    {
        return view("register");
    }

    public function admin(Request $request)
    {
        if ($request->input("out")) {
            return redirect("store/login");
        } else {
            $data["listBanner"] = BannerModel::all();
            $data["listGame"] = GameModel::all();
            $data["listV"] = VoucherModel::all();
            return view("admin", $data);
        }
    }

    public function processAdmin(Request $request)
    {
        if ($request->input("addGame")) {
            //dd($request->input());
            $this->validate($request, [
                'nama' => "required|unique:game,nama",
                'harga' => "required",
                'foto' => "required|mimes:jpg,png,jpeg"
            ]);
            $file = $request->file("foto");
            $filename = time() . ".jpg";
            $file->move(public_path('/img/foto_game'), $filename);

            $game = new GameModel();
            $game->nama = $request->input("nama");
            $game->harga = $request->input("harga");
            $game->deskripsi = $request->input("desc");
            $game->foto = $filename;
            $game->save();

            return redirect("store/admin");
        } else if ($request->input("addBanner")) {
            $this->validate($request, [
                'fotoBanner' => "required"
            ]);

            $file = $request->file("fotoBanner");
            $filename = time() . ".jpg";
            $file->move(public_path('/img/gameheader'), $filename);

            $banner = new BannerModel();
            $banner->foto = $filename;
            $banner->nama = $request->input("capBanner");
            $banner->save();

            return redirect("store/admin");
        } else if ($request->input("addVoucher")) {
            $this->validate($request, [
                "kode" => "required|unique:voucher,kode",
                "jml" => "required"
            ]);

            $voucher = new VoucherModel();
            $voucher->id = 0;
            $voucher->kode = $request->input("kode");
            $voucher->jumlah = $request->input("jml");
            $voucher->stat = "active";
            $voucher->save();

            return redirect("store/admin");
        } else if ($request->input("delete")) {
            $game = GameModel::where("nama", $request->input("nama"))->delete();
            return redirect("store/admin");
        } else if ($request->input('deleteBanner')) {

            $banner = BannerModel::where("id", $request->input("idBanner"))->delete();
            return redirect("store/admin");
        } else if ($request->input('deleteV')) {
            $voucher = VoucherModel::where("id", $request->input("idV"))->update(['stat' => 'non-active']);
            return redirect("store/admin");
        } else if ($request->input('actV')) {
            $voucher = VoucherModel::where("id", $request->input("idV"))->update(['stat' => 'active']);
            return redirect("store/admin");
        } else if ($request->input("editBanner")) {
            $id = $request->input("idBanner");
            $data["banner"] = BannerModel::where("id", $id)->get()->first();
            return view("editBanner",$data);
        }else if ($request->input("editGame")) {
            $nama = $request->input("nama");
            $data["game"] = GameModel::where("nama", $nama)->get()->first();
            return view("editGame",$data);
        }else {
            return redirect("store/admin");
        }
    }

    public function home(Request $request)
    {
        $user = Session::get("user");
        $data["listBanner"] = BannerModel::all();
        $data["listGame"] = GameModel::all();
        return view('home', $data);
    }

    public function library(Request $request)
    {
        if (Session::has("user")) {
            $user = Session::get("user");
            $data["library"] = LibraryModel::where("email_user", $user->email)->get();
            $data["game"] = GameModel::all();
            return view("library", $data);
        } else {
            return view("library");
        }
    }

    public function profile(Request $request)
    {
        if (Session::has("user")) {
            $user = Session::get('user');
            $hist = DB::table('history')->orderBy('id', 'desc')->where('user', $user->username)->get();
            return view("profile")->with(['hist' => $hist]);
        } else {
            return view("profile");
        }
    }

    public function topup(Request $request)
    {
        return view("topup");
    }

    public function cart(Request $request)
    {
        if (Session::has("cart")) {
            $total_price = 0;
            foreach (Session::get("cart") as $key) {
                $total_price = $total_price + $key->harga;
            }
            return view("cart")->with(["total" => $total_price]);
        } else {
            return view("cart");
        }
    }

    public function processCart(Request $request)
    {
        //code disini untuk tombol remove dan checkout...
        $user = Session::get("user");
        $game_name = $request->input("game_name");
        $total_price = 0;
        $cart = Session::get("cart");
        if ($request->input("remove")) {
            // $request->session()->forget('cart');
            // return view("cart")->withErrors(["Cart Empty"]);
            if (count($cart) == 1) {
                Session::forget("cart");
            } else {
                $idx = $request->input("index_game");
                $namagame = $request->input("game_name");
                //var_dump($cart);
                //$cart= array_splice($cart,$idx,1);
                unset($cart[$idx]);
                $cart = array_values($cart);
                //var_dump($cart);
                //Refresh Session Cart
                Session::put("cart", $cart);
            }
            return redirect("store/cart");
        } else if ($request->input("confirm")) {
            foreach (Session::get("cart") as $key) {
                $total_price = $total_price + $key->harga;
            }

            if ($user->balance >= $total_price) {
                //echo "UANG CUKUP!";
                $balanceNow = $user->balance - $total_price;
                DB::table('user')->where('username', $user->username)->update([
                    'balance' => $balanceNow
                ]);

                foreach (Session::get("cart") as $key) {
                    $library = new LibraryModel();
                    $library->nama_game = $key->nama;
                    $library->email_user = $user->email;
                    $library->tanggal_beli = date("d/m/Y");
                    $library->save();
                    DB::table('history')->insert([
                        'id' => 0,
                        'user' => $user->username,
                        'act' => "Purchased " . $key->nama,
                        'amt' => $key->harga,
                        'time' => date("d/m/Y")
                    ]);
                }

                $user->balance = $balanceNow;

                $data = array(
                    'name' => $user->username,
                    'email' => $user->email,
                    'bodyMessage' => "Thank you for your purchase! Here is your invoice!"
                );
                $dataInvoice = array(
                    "total" => $total_price,
                    "cart" => Session::get("cart"),
                    "user" => $user

                );
                $pdf = PDF::loadView('invoice', $dataInvoice)->setPaper('a4');
                Mail::send('email', $data, function ($message) use ($data, $pdf) {
                    $message->from("etoystorefai@gmail.com");
                    $message->to($data["email"]);
                    $message->subject('E-Toy Store Order Invoice');
                    $message->attachData($pdf->output(), 'invoice.pdf');
                });
                Session::forget("cart");
                Session::put("user", $user);
                return redirect("store/cart")->withErrors(["Purchase Complete!"]);
            } else {
                $total_price = 0;
                foreach (Session::get("cart") as $key)
                {
                    $total_price = $total_price + $key->harga;
                }
                return redirect("store/cart")->with(["total"=>$total_price])->withErrors(["Not Enough Balance!"]);
            }
        }
    }

    public function processTop(Request $request)
    {
        $user = Session::get('user');
        if ($request->input('topup')) {
            $nominal = $request->input('bal');
            $tipe= $request->input('ha');
            $bal = ($user->balance) + $nominal;
            $this->validate(
                $request,
                ['bal' => 'required',
                'ha'=>'required|in:mastercard,gopay,paypal'],
                ['bal.required' => 'Please Enter the Ammount to Topup',
                 'ha.required' => 'Please Select Payments Method']
            );
            UserModel::where('username', $user->username)->update(['balance' => $bal]);
            DB::table('history')->insert([
                'id' => 0,
                'user' => $user->username,
                'act'=>"Top Up from ".$tipe,
                'amt' => $nominal,
                'time' => date("d/m/Y")
            ]);
            $user = UserModel::where('username', $user->username)->first();
            Session::put('user', $user);
            return redirect('store/profile');
        } else if ($request->input('redeem')) {
            $kode = $request->input('code');
            $this->validate($request, [
                "code" => "required"
            ]);

            $voucher = VoucherModel::where('kode', $kode)->where('stat', 'active')->first();
            if ($voucher==null){                
                return redirect("store/topup")->withErrors('Wrong voucher code!');
            } else {
                $nominal = $voucher->jumlah;
                $bal = ($user->balance) + $nominal;
                $voucher = VoucherModel::where("kode", $kode)->update(['stat' => 'non-active']);
                UserModel::where('username', $user->username)->update(['balance' => $bal]);

                DB::table('history')->insert([
                    'id' => 0,
                    'user' => $user->username,
                    'act' => "Redeem",
                    'amt' => $nominal,
                    'time' => date("d/m/Y")
                ]);

                $user = UserModel::where('username', $user->username)->first();
                Session::put('user', $user);
                return redirect('store/profile');
            }
        } else if ($request->input('cancel')) {
            return redirect('store/profile');
        } else {
            return redirect('store/topup');
        }
    }

    public function editprofile(Request $request)
    {
        if ($request->input("home")) {
            return redirect("store/home");
        } else if ($request->input("lib")) {
            return redirect("store/library");
        } else if ($request->input("prof")) {
            return redirect("store/profile");
        } else { 
            $user= Session::get('user');
            $foto = $user->foto;
            $email = $user->email;
            $bio = $user->about;
            return view("editprofile", ["emailp" => $email, "fotop" => $foto, "biop" => $bio]);
        }
    }

    public function processEditProf(Request $request)
    {
        unset($errors);
        if ($request->input('save')) {
            $email = $request->input('edemail');
            $pass = $request->input('edpass');
            $foto = $request->input('foto');
            $bio = $request->input('edbio');
            $npass = $request->input('npass');
            $user = Session::get('user');
            if ($pass != null || $npass != null) {  
                unset($errors);
                if (Hash::check($pass, $user->password)) {
                    $this->validate($request, [
                        'edpass' => 'required',
                        'npass' => 'required',
                        'cnpass' => 'required|same:npass'
                    ], [
                        'edpass.required' => 'Enter Old Password!',
                        'npass.required' => 'Enter New Password!',
                        'cnpass.required' => 'Enter New Password Confirmation!',
                        'cnpass.same' => 'Please Check Your New Password Confirmation Again!'
                    ]);

                    UserModel::where('username', $user->username)->update([
                        'email' => $email,
                        'foto' => $foto,
                        'about' => $bio,
                        'password' => Hash::make($npass)
                    ]);

                    $user = UserModel::where('username', $user->username)->first();
                    Session::put('user', $user);
                    return redirect('store/profile');
                } else {
                    unset($errors);
                    return view("editprofile")->with(["fotop" => $foto, "emailp" => $email, "biop" =>$bio])->withErrors('Required Right Old Password!');                    
                }
            } else {
                UserModel::where('username', $user->username)->update([
                    'email' => $email,
                    'foto' => $foto,
                    'about' => $bio
                ]);

                $user = UserModel::where('username', $user->username)->first();
                Session::put('user', $user);
                return redirect('store/profile');
            }
        } else if ($request->input('up')) {
            $this->validate($request, ['upload' => 'required|mimes:jpg,png,jpeg']);
            $file = $request->file('upload');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('/img/profile'), $filename);
            $email = $request->input('edemail');
            $bio = $request->input('edbio');
            return view("editprofile", ["emailp" => $email, "fotop" => $filename, "biop" => $bio]);           
        } else if ($request->input('remove')) {
            $email = $request->input('edemail');
            $bio = $request->input('edbio');
            return view("editprofile", ["emailp" => $email, "fotop" => 'default.png', "biop" => $bio]);            
        } else if ($request->input('cancel')) {
            return redirect('store/profile');
        } else {
            return redirect('store/editprofile');
        }
    }

    public function processHeader(Request $request)
    {
        if ($request->input('lib')) {
            return redirect('store/library');
        } else if ($request->input('home')) {
            return redirect('store/home');
        } else if ($request->input('login')) {
            return redirect('store/login');
        } else if ($request->input('register')) {
            return redirect('store/register');
        } else if ($request->input('btnEdit')) {                
            return redirect('store/editprofile');
        } else if ($request->input('btnTop')) {
            return redirect('store/topup');
        } else if ($request->input('btnOut')) {
            Session::forget('user');
            Session::forget('cart');
            return redirect('store/home');
        } else if ($request->input('out')) {
            Session::forget('user');
            Session::forget('cart');
            return redirect('store/home');
        } else if ($request->input('prof')) {
            return redirect('store/profile');
        } else if ($request->input('cart')) {
            return redirect('store/cart');
        }
    }
    public function processHome(Request $request)
    {
        if ($request->input("nama")) {
            Session::put("namagame", $request->input("nama"));
            return redirect('store/detailGame');
        }
    }
    public function detailGame(Request $request)
    {
        $data["game"] = GameModel::where(["nama" => $request->session()->get('namagame', 'default')])->first();
        $data["cek"] = 0;
        $user = Session::get("user");

        if (Session::has("user")) {
            $library = LibraryModel::where("email_user", $user->email)->get();
            foreach ($library as $key) {
                if ($key->nama_game == $data["game"]->nama) {
                    $data["cek"] = 1;
                }
            }

            if (Session::has("cart")) {
                foreach (Session::get("cart") as $item) {
                    if ($item->nama == $data["game"]->nama) {
                        $data["cek"] = 2;
                    }
                }
            }
            return view("detailGame", $data);
        } else {
            $data["game"]->nama;
            return view("detailGame", $data);
        }
    }

    public function processDetailGame(Request $request)
    {
        if (Session::has("user")) {
            $nama_game = $request->input("nama_game");
            $game = GameModel::where("nama", $nama_game)->get()->first();
            Session::push("cart", $game);
            return redirect("store/detailGame");
        } else {
            return redirect("store/login");
        }
    }

    public function verification (Request $request){
        if (Session::has("userTemp")){
            $user = Session::get("userTemp");
            if($user->verification!=0){
                $a = rand(100000,999999);
                UserModel::where(["email"=>$user->email])->update(["verification"=>$a]);
                $data = array(
                    'email' => $user->email,
                    "kode"=>$a
                );
                Mail::send('verification_email', $data, function ($message) use ($data) {
                    $message->from("etoystorefai@gmail.com");
                    $message->to($data["email"]);
                    $message->subject('E-Toy Store verification code');
                });
                return view ("verification",["user"=>$user]);
            }
        }else{
            return redirect("store/login")->withErrors("Please log in first.");
        }

    }

    public function processVerification(Request $request){
        if($request->input("resend")){
            return redirect("verification");
        }else if($request->input("verifKode")){
            $user = Session::get("userTemp");
            $realUser = UserModel::where(["email"=>$user->email])->first();
            $trueCode =$realUser->verification;
            if($request->input("kode")==$trueCode){
                $a = UserModel::where("email",$user->email)->update(["verification"=>0]);
                Session::forget("userTemp");
                $user = UserModel::where(["email"=>$user->email])->first();
                Session::put("user",$user);
                return redirect("store/home");
            }else{
                return redirect("store/verification");
            }
        }
    }

    public function processEdBanner(Request $request)
    {
        if ($request->input("updateBanner"))
        {
            $this->validate($request, [
                'namaBanner' => "required",
                'fotoBanner' => "mimes:jpg,png,jpeg"
            ]);

            $file = $request->file("fotoBanner");
            if ($file==null)
            {
                BannerModel::where("id", $request->input("idBanner"))->update([
                    "nama" => $request->input("namaBanner")
                ]);
            }
            else
            {
                $filename = time() . ".jpg";
                $file->move(public_path('/img/gameheader'), $filename);
                BannerModel::where("id", $request->input("idBanner"))->update([
                    "nama" => $request->input("namaBanner"),
                    "foto" => $filename
                ]);
            }
        }
        else if ($request->input("cancel")){
            return redirect("store/admin");
        }                    
        return redirect("store/admin");
    }
    public function edGame(Request $request)
    {
        return view("editGame");
    }

    public function processEdGame(Request $request)
    {
        if ($request->input("updateGame"))
        {
            $this->validate($request, [
                'hargaGame' => "required",
                'fotoGame' => "mimes:jpg,png,jpeg"
            ]);

            $file = $request->file("fotoGame");
            if ($file==null)
            {
                GameModel::where("nama", $request->input("namaGame"))->update([
                    "harga" => $request->input("hargaGame"),
                    "deskripsi" => $request->input("descGame")
                ]);
            }
            else
            {
                $filename = time() . ".jpg";
                $file->move(public_path('/img/foto_game'), $filename);
                GameModel::where("nama", $request->input("namaGame"))->update([
                    "harga" => $request->input("hargaGame"),
                    "deskripsi" => $request->input("descGame"),
                    "foto" => $filename
                ]);
            }        
        }
        else if ($request->input("cancel")){
            return redirect("store/admin");
        } 
        return redirect("store/admin");
    }
}
