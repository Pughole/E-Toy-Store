<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = "user";
    protected $connection = "mysql";
    public $timestamps = false;
    protected $fillable = [
        'username',
        'email',
        'password',
        'foto',
        'balance',
        'lib',
        'verification'
    ];

}
