<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VoucherModel extends Model
{
    protected $table = "voucher";
    protected $primaryKey = "id";
    protected $connection = "mysql";
    public $timestamps = false;
    protected $fillable = [
        'id',
        'kode',
        'jumlah',
        'stat'        
    ];
}
