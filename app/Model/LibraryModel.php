<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LibraryModel extends Model
{
    protected $table = "library";
    protected $connection = "mysql";
    protected $primaryKey = "id_library";
    public $timestamps = false;
    protected $fillable = [
        'nama_game',
        'email_user',
        'tanggal_beli'
    ];

}
