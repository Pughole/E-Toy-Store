<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view("welcome");
});

Route::group(['prefix' => 'store'], function () {
    Route::get('login', "MainController@login");
    Route::post("processLogin", 'MainController@processLogin');

    Route::get('register', "MainController@register");
    Route::post("processRegister", 'MainController@processRegister');

    Route::get("home", 'MainController@home');
    Route::post("processHome", 'MainController@processHome');

    Route::get("library", 'MainController@library');

    Route::get("profile", 'MainController@profile');

    Route::get("topup", 'MainController@topup');
    Route::Post("processTop", 'MainController@processTop');

    Route::get("admin", 'MainController@admin');
    Route::post("processAdmin", 'MainController@processAdmin');

    Route::any("editprofile", 'MainController@editprofile');
    Route::post("processEditProf", 'MainController@processEditProf');

    Route::post("processHeader", "MainController@processHeader");

    Route::get('detailGame', "MainController@detailGame");
    Route::post('processDetailGame', "MainController@processDetailGame");

    Route::get('cart',"MainController@cart");
    Route::post('processCart',"MainController@processCart");

    Route::get('verification',"MainController@verification");
    Route::post('processVerification',"MainController@processVerification");

    Route::post('processEdGame',"MainController@processEdGame");

    Route::post('processEdBanner',"MainController@processEdBanner");
});
