-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2020 at 05:52 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyek`
--
CREATE DATABASE IF NOT EXISTS `proyek` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `proyek`;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(8) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `foto` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `nama`, `foto`) VALUES
(7, 'Pokemon FireRed', '1578243103.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `nama` varchar(128) NOT NULL,
  `harga` int(10) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `foto` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`nama`, `harga`, `deskripsi`, `foto`) VALUES
('CSGO', 175000, 'Bergabung bersama teman-temanmu di arena perang sekarang!', '1577643292.jpg'),
('Mario Kart', 50000, 'Game balapan bersama mario dan kawan-kawan!', '1577631138.jpg'),
('Pokemon FireRed', 150000, 'Tangkap Pokemon yang kamu inginkan dan selesaikan misinya di pokemon firered!', '1577633408.jpg'),
('PUBG', 200000, 'Tembak-tembakan asik bersama teman-temanmu, hanya di PUBG!', '1577637179.jpg'),
('Spyro Reignited Trilogy', 125000, 'Bertualanglah bersama spyro dan kalahkan musuh-musuh jahat', '1577638773.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `user` varchar(200) NOT NULL,
  `act` varchar(100) NOT NULL,
  `amt` int(11) NOT NULL,
  `time` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `user`, `act`, `amt`, `time`) VALUES
(1, 'as', 'Purchased Mario Kart', 50000, '30/12/2019'),
(2, 'as', 'Top Up', 300000, '30/12/2019'),
(3, 'as', 'Purchased CSGO', 175000, '30/12/2019'),
(4, 'as', 'Purchased PUBG', 200000, '30/12/2019'),
(5, 'as', 'Top Up', 50000, '30/12/2019 13:32'),
(6, 'as', 'Top Up', 50000, '02/01/2020'),
(7, 'as', 'Redeem', 200000, '02/01/2020'),
(8, 'ivan', 'Top Up', 1500000, '05/01/2020'),
(9, 'ivan', 'Purchased CSGO', 175000, '05/01/2020'),
(10, 'ivan', 'Purchased Mario Kart', 50000, '05/01/2020'),
(11, 'ivan', 'Purchased Mario Kart', 50000, '05/01/2020'),
(12, 'ivan', 'Purchased Mario Kart', 50000, '05/01/2020'),
(13, 'ivan', 'Purchased Pokemon FireRed', 150000, '05/01/2020'),
(14, 'ivan', 'Purchased PUBG', 200000, '05/01/2020'),
(15, 'ivan', 'Purchased Spyro Reignited Trilogy', 125000, '05/01/2020'),
(16, 'ivan', 'Purchased CSGO', 175000, '05/01/2020'),
(17, 'ivan', 'Purchased Mario Kart', 50000, '05/01/2020'),
(18, 'ivan', 'Purchased Pokemon FireRed', 150000, '05/01/2020'),
(19, 'ivan', 'Purchased PUBG', 200000, '05/01/2020'),
(20, 'ivan', 'Purchased Spyro Reignited Trilogy', 125000, '05/01/2020'),
(21, 'ivan', 'Top Up', 1000000, '05/01/2020'),
(22, 'ivan', 'Purchased CSGO', 175000, '05/01/2020'),
(23, 'ivan', 'Purchased Mario Kart', 50000, '05/01/2020'),
(24, 'ivan', 'Purchased Pokemon FireRed', 150000, '05/01/2020'),
(25, 'ivan', 'Purchased PUBG', 200000, '05/01/2020'),
(26, 'ivan', 'Purchased CSGO', 175000, '05/01/2020'),
(27, 'ivan', 'Purchased Mario Kart', 50000, '05/01/2020'),
(28, 'ivan', 'Purchased Spyro Reignited Trilogy', 125000, '05/01/2020'),
(29, 'ivan', 'Top Up', 2000000, '05/01/2020'),
(30, 'ivan', 'Purchased PUBG', 200000, '05/01/2020'),
(31, 'ivan', 'Purchased PUBG', 200000, '05/01/2020'),
(32, 'ivan', 'Purchased PUBG', 200000, '05/01/2020');

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE `library` (
  `id_library` int(6) NOT NULL,
  `nama_game` varchar(128) NOT NULL,
  `email_user` varchar(32) NOT NULL,
  `tanggal_beli` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`id_library`, `nama_game`, `email_user`, `tanggal_beli`) VALUES
(1, 'Mario Kart', 'a@gmail.com', '29/12/2019'),
(2, 'Pokemon FireRed', 'a@gmail.com', '30/12/2019'),
(5, 'Pokemon FireRed', 'v@gmail.com', '29/12/2019'),
(6, 'Spyro Reignited Trilogy', 'v@gmail.com', '29/12/2019'),
(8, 'Mario Kart', 'v@gmail.com', '29/12/2019'),
(9, 'Pokemon FireRed', 'ab@gmail.com', '30/12/2019'),
(10, 'Mario Kart', 'ab@gmail.com', '30/12/2019'),
(11, 'CSGO', 'ab@gmail.com', '30/12/2019'),
(12, 'PUBG', 'ab@gmail.com', '30/12/2019'),
(13, 'Mario Kart', 'as@yahoo.com', '30/12/2019'),
(14, 'CSGO', 'as@yahoo.com', '30/12/2019'),
(15, 'PUBG', 'as@yahoo.com', '30/12/2019'),
(32, 'CSGO', 'ivanchristian8a@gmail.com', '05/01/2020'),
(33, 'Mario Kart', 'ivanchristian8a@gmail.com', '05/01/2020'),
(34, 'Spyro Reignited Trilogy', 'ivanchristian8a@gmail.com', '05/01/2020'),
(37, 'PUBG', 'ivanchristian8a@gmail.com', '05/01/2020');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `foto` varchar(32) NOT NULL,
  `about` varchar(200) NOT NULL,
  `balance` int(8) NOT NULL,
  `lib` varchar(512) NOT NULL,
  `verification` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `email`, `password`, `foto`, `about`, `balance`, `lib`, `verification`) VALUES
('ivan', 'ivanchristian8a@gmail.com', '$2y$10$G.8AwOzJ6rDRY.HaH8Zaxu0DO9umzTDVVIM9Fk16q0RYLR5.0zNSK', 'default.png', 'Hello!', 1575000, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `stat` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `kode`, `jumlah`, `stat`) VALUES
(1, '111', 50000, 'active'),
(2, '333', 200000, 'non-active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`nama`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`id_library`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD UNIQUE KEY `balance` (`username`,`email`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `library`
--
ALTER TABLE `library`
  MODIFY `id_library` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
